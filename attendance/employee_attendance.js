const Attendance = require("../models/attendance");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");
const User = require("../models/user");

const removeValues = {
  isCheckIn: 0,
  isCheckOut: 0,
  _id: 0,
  employee_id: 0,
  createdAt: 0,
  updatedAt: 0,
  __v: 0,
};

const employeeAttendance = async (req, res, next) => {
  if (!req.body.fromDate || !req.body.toDate) {
    return res
      .status(Constants.RESPONSE_CODES.BAD_REQUEST)
      .send(
        ResponseTemplate.badRequestTemplate(
          "provide correct payload",
          Constants.RESPONSE_CODES.BAD_REQUEST
        )
      );
  }

  var date = new Date().toJSON().slice(0, 10);
  var currentDate =
    date.slice(8, 10) + "/" + date.slice(5, 7) + "/" + date.slice(0, 4);

  var fromDate = req.body.fromDate;
  var fromdate = fromDate.split("/");
  const oneDay = 1000 * 60 * 60 * 24;

  var newFromDate = fromdate[1] + "/" + fromdate[0] + "/" + fromdate[2];

  var toDate = req.body.toDate;
  var todate = toDate.split("/");

  var newToDate = todate[1] + "/" + todate[0] + "/" + todate[2];

  const date1 = new Date(newFromDate);
  const date2 = new Date(newToDate);

  const diffInTime = date2.getTime() - date1.getTime();

  const diffInDays = Math.round(diffInTime / oneDay) + 1;

  try {
    const fetchAttendance = await Attendance.find(
      {
        employee_id: req.params.id,
        date: {
          $gte: req.body.fromDate || currentDate,
          $lte: req.body.toDate || currentDate,
        },
      },
      removeValues
    ).lean();

    const userDetails = await User.findOne({ employee_id: req.params.id });

    if (!userDetails)
      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(ResponseTemplate.dataTemplate({}, `User not found.`));

    if (!fetchAttendance.length)
      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(ResponseTemplate.successTemplate({}, `No Attendance found.`));

    const fullPresent = fetchAttendance.filter(
      (details) => details.status === "full day"
    ).length;
    const halfPresent = fetchAttendance.filter(
      (details) => details.status === "half day"
    ).length;
    const leaveDays = fetchAttendance.filter(
      (details) => details.status === "Leave"
    ).length;

    const presentDays = fullPresent + halfPresent / 2;

    fetchAttendance[0].NumberOfDays = diffInDays;
    fetchAttendance[0].present = presentDays;
    fetchAttendance[0].absent = leaveDays;
    fetchAttendance[0].name = userDetails.name;

    if (fetchAttendance) {
      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(
          ResponseTemplate.successTemplate(
            fetchAttendance,
            `Attendance from ${req.body.fromDate} to ${req.body.toDate}.`
          )
        );
    } else {
      let err = new Error(`Something went wrong.`);
      err.status = Constants.RESPONSE_CODES.BAD_REQUEST;
      throw err;
    }
  } catch (err) {
    res
      .status(err.status || 400)
      .send(
        ResponseTemplate.badRequestTemplate(err.message, err.status || 400)
      );
  }
};

module.exports = {
  employeeAttendance,
};
