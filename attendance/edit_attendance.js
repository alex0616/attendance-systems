const Attendance = require("../models/attendance");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");
const User = require("../models/user");
const { editAttendanceLog } = require("../utils/activity_log");

const editAttendance = async (req, res, next) => {
  if (req.user.role !== "admin")
    return res
      .status(Constants.RESPONSE_CODES.UN_AUTHORIZED)
      .send(
        ResponseTemplate.badRequestTemplate(
          Strings.ERROR_MESSAGES.NOT_AUTH,
          Constants.RESPONSE_CODES.UN_AUTHORIZED
        )
      );

  if (!req.body.date || !req.body.employeeId)
    return res
      .status(Constants.RESPONSE_CODES.UN_AUTHORIZED)
      .send(
        ResponseTemplate.badRequestTemplate(
          `Please Provide sufficent data`,
          Constants.RESPONSE_CODES.UN_AUTHORIZED
        )
      );

  const payLoad = {
    checkIn: "09.30",
    date: req.body.date,
    status: req.body.status,
    employee_id: req.body.employeeId,
    isCheckIn: true,
    isCheckOut: true,
  };

  try {
    if (req.body.employeeId && req.body.date && !req.body.status) {
      const user = req.body;

      const attendanceDetails = await Attendance.findOne({
        employee_id: req.body.employeeId,
        date: req.body.date,
      });

      if (!attendanceDetails) {
        return res
          .status(Constants.RESPONSE_CODES.REQUEST_OK)
          .send(
            ResponseTemplate.successTemplate(
              {},
              `No Attendance found for ${req.body.date}.`
            )
          );
      } else {
        return res
          .status(Constants.RESPONSE_CODES.REQUEST_OK)
          .send(
            ResponseTemplate.successTemplate(
              attendanceDetails,
              `Attendance  for ${req.body.date}.`
            )
          );
      }
    } else if (req.body.employeeId && req.body.date && req.body.status) {
      if (req.body.status === "full day") {
        payLoad.checkOut = "18.00";
      } else if (req.body.status === "half day") {
        payLoad.checkOut = "13.30";
      } else if (req.body.status === "leave") {
        payLoad.checkIn = "-";
        payLoad.checkOut = "-";
        payLoad.isCheckIn = false;
        payLoad.isCheckOut = false;
      } else {
        let err = new Error("Incorrect value");
        err.status = Constants.RESPONSE_CODES.BAD_REQUEST;
        throw err;
      }

      const findUser = await User.findOne({ employee_id: req.body.employeeId });

      if (!findUser)
        return res
          .status(Constants.RESPONSE_CODES.BAD_REQUEST)
          .send(
            ResponseTemplate.badRequestTemplate(
              `User not found`,
              Constants.RESPONSE_CODES.BAD_REQUEST
            )
          );

      const edit = await Attendance.update(
        { employee_id: req.body.employeeId, date: req.body.date },
        { $set: payLoad },
        { upsert: true }
      );

      const employeeDetails = await User.findOne({
        employee_id: req.body.employeeId,
      });

      await editAttendanceLog(
        req.user.name,
        employeeDetails.name,
        employeeDetails.employee_id,
        req.body.date
      );

      if (edit)
        return res
          .status(Constants.RESPONSE_CODES.REQUEST_OK)
          .send(
            ResponseTemplate.successTemplate(
              {},
              `Attendance edited for ${req.body.date}.`
            )
          );
    }
  } catch (err) {
    res
      .status(err.status || 400)
      .send(
        ResponseTemplate.badRequestTemplate(err.message, err.status || 400)
      );
  }
};
module.exports = {
  editAttendance,
};
