const Attendance = require("../models/attendance");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");
const User = require("../models/user");

var date = new Date().toJSON().slice(0, 10);
var currentDate =
  date.slice(8, 10) + "/" + date.slice(5, 7) + "/" + date.slice(0, 4);

const addDetails = (data) => {
  data.reporter_id = data.reporter_id.name;
  data.designation = data.designation.designation_type;

  if (data.idealDate != currentDate || false) {
    data.status = "checkIn pending";
  }

  return data;
};

const removeValues = (data) => {
  delete data.email;
  delete data.salt;
  delete data.encry_password;
  delete data.phoneNumber;
  delete data.age;
  return data;
};

const ITEMS_PER_PAGE = 5;

const viewAttendance = async (req, res, next) => {
  let count;
  let getEmployees;
  try {
    const page = req.query.page || 1;

    console.log(page);

    if (req.user.role === "reporter") {
      if (req.body.name) {
        count = await User.find({
          $and: [
            { name: new RegExp(req.body.name) },
            { reporter_id: req.user._id },
          ],
        }).countDocuments();

        console.log(count);

        getEmployees = await User.find({
          $and: [
            { name: new RegExp(req.body.name) },
            { reporter_id: req.user._id },
          ],
        })
          .lean()
          .skip((page - 1) * ITEMS_PER_PAGE)
          .limit(ITEMS_PER_PAGE)
          .populate("reporter_id")
          .populate("designation");
      } else {
        getEmployees = await User.find({
          $or: [{ reporter_id: req.user._id }, { _id: req.user._id }],
        })
          .lean()
          .skip((page - 1) * ITEMS_PER_PAGE)
          .limit(ITEMS_PER_PAGE)
          .populate("reporter_id")
          .populate("designation");

        count = await User.find({
          $or: [{ reporter_id: req.user._id }, { _id: req.user._id }],
        }).countDocuments();
      }
    } else if (req.user.role === "admin") {
      if (req.body.name) {
        getEmployees = await User.find({ name: new RegExp(req.body.name) })
          .lean()
          .skip((page - 1) * ITEMS_PER_PAGE)
          .limit(ITEMS_PER_PAGE)
          .populate("reporter_id")
          .populate("designation");

        count = await User.find({
          name: new RegExp(req.body.name),
        }).countDocuments();
      } else {
        getEmployees = await User.find({ _id: { $ne: req.user._id } })
          .lean()
          .skip((page - 1) * ITEMS_PER_PAGE)
          .limit(ITEMS_PER_PAGE)
          .populate("reporter_id")
          .populate("designation");

        count = await User.find({
          _id: { $ne: req.user._id },
        }).countDocuments();
      }
    } else {
      let err = new Error("You don't have access to use this.");
      err.status = Constants.RESPONSE_CODES.UN_AUTHORIZED;
      throw err;
    }

    const addedDetails = getEmployees.map((details) => addDetails(details));

    const formattedUser = addedDetails.map((values) => removeValues(values));
    
    if (!formattedUser.length) {
      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(
          ResponseTemplate.customDataTemplate({}, count, "No results found")
        );
    } else {
      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(
          ResponseTemplate.customDataTemplate(formattedUser, count, "Employees")
        );
    }
  } catch (err) {
    console.log(err);
    res
      .status(err.status || 400)
      .send(
        ResponseTemplate.badRequestTemplate(err.message, err.status || 400)
      );
  }
};

module.exports = {
  viewAttendance,
};
