const Attendance = require("../models/attendance");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");
const { checkInLog, checkOutLog, leaveLog } = require("../utils/activity_log");
const User = require("../models/user");

const postAttendance = async (req, res, next) => {
  var date = new Date().toJSON().slice(0, 10);
  var currentDate =
    date.slice(8, 10) + "/" + date.slice(5, 7) + "/" + date.slice(0, 4);

  var currentTime = new Date().getHours() + ":" + new Date().getMinutes();

  let status;

  let checkattendance = false;

  try {
    const checkAttendance = await Attendance.findOne({
      employee_id: req.user.employeeId,
      date: currentDate,
    });
    const user = await User.findOne({ _id: req.user._id });

    if (checkAttendance) checkattendance = true;

    if (req.body.checkIn) {
      if (checkattendance) {
        let err = new Error(`Already checkedIn for ${currentDate}.`);
        err.status = Constants.RESPONSE_CODES.BAD_REQUEST;
        throw err;
      }

      const checkIn = await Attendance.create({
        date: currentDate,
        checkIn: currentTime,
        employee_id: req.user.employeeId,
        isCheckIn: true,
      });

      (user.ideal = "checkIn"),
        (user.idealDate = currentDate),
        (user.status = "checkOut pending");

      await user.save();

      await checkInLog(user.name, user.employee_id);

      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(
          ResponseTemplate.successTemplate(
            currentTime,
            `checkIn for ${currentDate} is successful.`
          )
        );
    } else if (req.body.checkOut) {
      if (!checkattendance) {
        let err = new Error(`please do checkIn first for ${currentDate}.`);
        err.status = Constants.RESPONSE_CODES.BAD_REQUEST;
        throw err;
      }

      const attendance = await Attendance.findOne({
        employee_id: req.user.employeeId,
        date: currentDate,
      });

      if (attendance.checkOut) {
        let err = new Error(`Already checkedOut for ${currentDate}.`);
        err.status = Constants.RESPONSE_CODES.BAD_REQUEST;
        throw err;
      }

      if (attendance.checkIn <= "09.30" && currentTime >= "18.00") {
        status = "full day";
      } else if (attendance.checkIn <= "13.30" && currentTime >= "18.00") {
        status = "half day";
      } else if (attendance.checkIn <= "09.30" && currentTime >= "13.30") {
        status = "half day";
      } else {
        status = "leave";
      }

      attendance.status = status;
      attendance.checkOut = currentTime;
      attendance.isCheckOut = true;

      await attendance.save();

      (user.ideal = "checkOut"),
        (user.idealDate = currentDate),
        (user.status = status);

      await user.save();

      await checkOutLog(user.name, user.employee_id);

      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(
          ResponseTemplate.successTemplate(
            currentTime,
            `checkOut for ${currentDate} is successful.`
          )
        );
    } else if (req.body.leave) {
      if (checkattendance) {
        let err = new Error(
          `Already checkedIn for ${currentDate} can't mark Leave.`
        );
        err.status = Constants.RESPONSE_CODES.BAD_REQUEST;
        throw err;
      }

      const leave = await Attendance.create({
        date: currentDate,
        checkIn: "-",
        checkOut: "-",
        status: "Leave",
        employee_id: req.user.employeeId,
      });

      (user.ideal = "leave"),
        (user.idealDate = currentDate),
        (user.status = "leave");

      await user.save();
      await leaveLog(user.name, user.employee_id);

      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(
          ResponseTemplate.successTemplate(
            {},
            `Marked Leave for ${currentDate}.`
          )
        );
    } else {
      let err = new Error(`The format is incorrect or something went wrong.`);
      err.status = Constants.RESPONSE_CODES.BAD_REQUEST;
      throw err;
    }
  } catch (err) {
    res
      .status(err.status || 400)
      .send(
        ResponseTemplate.badRequestTemplate(err.message, err.status || 400)
      );
  }
};

module.exports = {
  postAttendance,
};
