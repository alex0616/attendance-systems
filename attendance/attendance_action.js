const Attendance = require("../models/attendance");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");
const User = require("../models/user");

var date = new Date().toJSON().slice(0, 10);
var currentDate =
  date.slice(8, 10) + "/" + date.slice(5, 7) + "/" + date.slice(0, 4);

const attendanceButton = async (req, res, next) => {
  let data = {};

  const attendance = await Attendance.findOne({
    employee_id: req.user.employeeId,
    date: currentDate,
  });

  if (!attendance) {
    data.checkIn = false;
    data.checkOut = false;
    data.leave = false;
    return res
      .status(200)
      .send(ResponseTemplate.successTemplate(data, `Do checkIn.`));
  } else if (attendance.checkIn && attendance.status === "pending") {
    data.checkIn = true;
    data.checkout = false;
    data.leave = false;
    return res
      .status(200)
      .send(ResponseTemplate.successTemplate(data, `Do checkOut.`));
  } else if (
    (attendance.checkIn &&
      attendance.checkOut &&
      attendance.status === "full day") ||
    "half day"
  ) {
    data.checkIn = true;
    data.checkOut = true;
    data.leave = false;
    return res
      .status(200)
      .send(ResponseTemplate.successTemplate(data, `Attendance marked.`));
  } else if (attendance.status === "leave") {
    data.checkIn = false;
    data.checkOut = false;
    data.leave = true;
    return res
      .status(200)
      .send(ResponseTemplate.successTemplate(data, `take rest.`));
  } else {
    data.checkIn = true;
    data.checkOut = true;
    data.leave = false;
    return res
      .status(200)
      .send(ResponseTemplate.successTemplate(data, `Attendance maybe marked.`));
  }
};

module.exports = {
  attendanceButton,
};
