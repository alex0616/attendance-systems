const Activity = require("../models/activity_log");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");

const ITEMS_PER_PAGE = 5;

const getActivity = async (req, res, next) => {
  if (req.user.role !== "admin")
    return res
      .status(Constants.RESPONSE_CODES.UN_AUTHORIZED)
      .send(
        ResponseTemplate.badRequestTemplate(
          Strings.ERROR_MESSAGES.NOT_AUTH,
          Constants.RESPONSE_CODES.UN_AUTHORIZED
        )
      );

  const user = req.body;

  const page = req.query.page || 1;

  let dateFormat = {
    $gte: req.body.fromDate,
    $lte: req.body.toDate,
  };

  try {
    if (req.body.fromDate && req.body.toDate === null) {
      dateFormat = {
        $gte: req.body.fromDate,
        $lte: req.body.fromDate,
      };

      (user.date = dateFormat), delete user.fromDate;
    } else if (req.body.fromDate) {
      user.date = dateFormat;
      delete user.fromDate, delete user.toDate;
    } else if (req.body.toDate) {
      dateFormat = {
        $gte: req.body.toDate,
        $lte: req.body.toDate,
      };

      (user.date = dateFormat), delete user.toDate;
    }

    Object.entries(user).forEach((o) =>
      o[1] === null ? delete user[o[0]] : 0
    );

    const activityLog = await Activity.find(user, { _id: 0, sentence: 1 })
      .sort({ createdAt: -1 })
      .skip((page - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE);

    const count = await Activity.find(user).countDocuments();

    if (!activityLog.length) {
      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(ResponseTemplate.dataTemplate({}, `No results found.`));
    } else {
      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(
          ResponseTemplate.customDataTemplate(
            activityLog,
            count,
            `Activity logs.`
          )
        );
    }
  } catch (err) {
    res
      .status(err.status || 400)
      .send(
        ResponseTemplate.badRequestTemplate(err.message, err.status || 400)
      );
  }
};

module.exports = {
  getActivity,
};
