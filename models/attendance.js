const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const AttendanceSchema = new Schema(
  {
    date: {
      type: String,
      required: true,
    },
    checkIn: {
      type: String,
      default: null,
    },
    checkOut: {
      type: String,
      default: null,
    },
    employee_id: {
      type: Number,
      required: true,
    },
    status: {
      type: String,
      default: "pending",
    },
    isCheckIn: {
      type: Boolean,
      default: false,
    },
    isCheckOut: {
      type: Boolean,
      default: false,
    },
  },
  { collection: "attendance", timestamps: true }
);

module.exports = mongoose.model("attendance", AttendanceSchema);
