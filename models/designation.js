const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const DesignationSchema = new Schema(
  {
    designation_type: {
      type: String,
      required: true,
    },
  },
  { collection: "designation" }
);

module.exports = mongoose.model("designation", DesignationSchema);
