const mongoose = require("mongoose");
const crypto = require("crypto");
const { v4: uuidv4 } = require("uuid");
const AutoIncrement = require("mongoose-sequence")(mongoose);
const Schema = mongoose.Schema;

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    require: true,
    maxlength: 32,
    trim: true,
  },
  email: {
    type: String,
    require: true,
    unique: true,
  },
  phoneNumber: {
    type: Number,
    require: true,
    unique: true,
    trim: true,
    maxlength: 10,
  },
  employee_id: {
    type: Number,
  },
  designation: {
    type: Schema.Types.ObjectId,
    require: true,
    ref: "designation",
  },
  role: {
    type: String,
    required: true,
  },
  reporter_id: {
    type: Schema.Types.ObjectId,
    // required: true,
    ref: "User",
  },
  ideal: {
    type: String,
    default: false,
  },
  idealDate: {
    type: String,
    default: false,
  },
  status: {
    type: String,
    default: true,
  },
  age: {
    type: Number,
    require: true,
    trim: true,
    maxlength: 10,
  },
  encry_password: {
    type: String,
    require: true,
  },
  salt: String,
  // assignedPerm: {
  //     type: [],
  //     default: ["view"]
  // },
  // isAdmin: {
  //   type: Boolean,
  //   default: false
  // }
});

userSchema.plugin(AutoIncrement, { inc_field: "employee_id" });

userSchema
  .virtual("password")
  .set(function (password) {
    this._password = password;
    this.salt = uuidv4();
    this.encry_password = this.securePassword(password);
  })
  .get(function () {
    return this._password;
  });

userSchema.methods = {
  authendicate: function (planepassword) {
    return this.securePassword(planepassword) === this.encry_password;
  },

  securePassword: function (planepassword) {
    if (!planepassword) {
      return "";
    }
    try {
      return crypto
        .createHmac("sha256", this.salt)
        .update(planepassword)
        .digest("hex");
    } catch (err) {
      return "";
    }
  },
};

module.exports = mongoose.model("User", userSchema);
