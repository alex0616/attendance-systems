const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ActivitySchema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    employee_id: {
      type: String,
      required: true,
    },
    date: {
      type: String,
      required: true,
    },
    action: {
      type: String,
      required: true,
    },
    sentence: {
      type: String,
      required: true,
    },
  },
  { collection: "activity log", timestamps: true }
);

module.exports = mongoose.model("activity", ActivitySchema);
