const express = require("express");

const router = express.Router();

const auth = require("../utils/auth");

const { check } = require("express-validator");

const { getReporters } = require("../controllers/users");

const { editUser } = require("../user/edit_user");

const { deleteUser } = require("../user/delete_user");

const { userList } = require("../user/get_user");

const { getUserName } = require("../user/get_user_name");

router.get("/get-reporters", getReporters);

router.put(
  "/user/:userId",
  [
    check("name")
      .isLength({ min: 3 })
      .withMessage("name Should be atleast 3 char"),
    check("email").isEmail().withMessage("Please enter a valid email"),
    check("phoneNumber")
      .isLength({ min: 10, max: 10 })
      .withMessage("Phone Number should be 10 letter long and not exceed")
      .isNumeric()
      .withMessage("should be numeric"),
    check("age")
      .isNumeric()
      .withMessage("Please use numbers for age ")
      .isLength({ max: 2 })
      .withMessage("cannot proceed a age more than 100"),
  ],
  auth,
  editUser
);

router.delete("/user/:id", auth, deleteUser);

router.get("/users", auth, userList);

router.get("/get-user-name", getUserName);

module.exports = router;
