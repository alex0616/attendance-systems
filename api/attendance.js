const express = require("express");

const router = express.Router();

const auth = require("../utils/auth");

const { attendanceButton } = require("../attendance/attendance_action");

const { postAttendance } = require("../attendance/post_attendance");

const { employeeAttendance } = require("../attendance/employee_attendance");

const { viewAttendance } = require("../attendance/view_attendance");

const { editAttendance } = require("../attendance/edit_attendance");

router.get("/attendance-action", auth, attendanceButton);

router.post("/post-attendance", auth, postAttendance);

router.post("/employee-attendance/:id", auth, employeeAttendance);

router.post("/view-attendance", auth, viewAttendance);

router.post("/edit-attendance", auth, editAttendance);

module.exports = router;
