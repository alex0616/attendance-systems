const express = require("express");

const router = express.Router();

const auth = require("../utils/auth");

const { getActivity } = require("../activity_log/activity_log");

router.post("/get-activity", auth, getActivity);

module.exports = router;
