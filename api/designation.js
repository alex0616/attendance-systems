const express = require("express");

const router = express.Router();

const {
  getDesignation,
  postDesignation,
} = require("../controllers/designation");

router.get("/get-designation", getDesignation);

router.post("/post-designation", postDesignation);

module.exports = router;
