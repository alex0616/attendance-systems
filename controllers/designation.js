const Designation = require("../models/designation");

const getDesignation = async (req, res, next) => {
  const designation = await Designation.find();

  if (!designation)
    return res.status(502), json({ msg: "db operation failed" });

  return res.status(200).json({ data: designation });
};

const postDesignation = async (req, res, next) => {
  const createDesignation = await Designation.create({
    designation_type: req.body.designation_type,
  });

  if (!createDesignation)
    return res.status(400).json({ msg: "db operation failed" });

  return res.status(200).json({
    msg: "Designation created",
    designation: req.body.designation_type,
  });
};

module.exports = {
  getDesignation,
  postDesignation,
};
