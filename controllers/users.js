const User = require("../models/user");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");

const removeValues = {
  email: 0,
  phoneNumber: 0,
  designation: 0,
  role: 0,
  reporter_id: 0,
  age: 0,
  encry_password: 0,
  salt: 0,
  __v: 0,
};

const getReporters = async (req, res, next) => {
  const reporters = await User.find({ role: "reporter" }, removeValues);

  return res
    .status(200)
    .send(ResponseTemplate.successTemplate(reporters, "List of reporters"));
};

module.exports = {
  getReporters,
};
