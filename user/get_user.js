const User = require("../models/user");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");

const removeValues = {
  email: 0,
  phoneNumber: 0,
  designation: 0,
  role: 0,
  reporter_id: 0,
  age: 0,
  encry_password: 0,
  salt: 0,
  __v: 0,
};

const ITEMS_PER_PAGE = 5;

const getDesignationReporter = (data) => {
  data.Designation = data.designation.designation_type;
  data.Reporter = data.reporter_id.name;
  return data;
};

const deleteDate = (data) => {
  delete data.designation,
    delete data.reporter_id,
    delete data.salt,
    delete data.encry_password;
  delete data.__v;
  return data;
};

const getReporters = async (req, res, next) => {
  const reporters = await User.find({ role: "reporter" }, removeValues);
  return res
    .status(200)
    .send(ResponseTemplate.successTemplate(reporters, "List of reporters"));
};

const userList = async (req, res) => {
  try {
    const page = req.query.page || 1;

    if (req.user.role !== "admin") {
      let err = new Error(`You are not allowed`);
      err.status = Constants.RESPONSE_CODES.UN_AUTHORIZED;
      throw err;
    }

    const user = await User.find({ _id: { $ne: req.params.userId } })
      .lean()
      .skip((page - 1) * ITEMS_PER_PAGE)
      .limit(ITEMS_PER_PAGE)
      .populate("designation")
      .populate("reporter_id", { name: 1 });

    const addDesignationReporter = user.map((details) =>
      getDesignationReporter(details)
    );

    const formattedData = addDesignationReporter.map((data) =>
      deleteDate(data)
    );

    if (!user) return res.status(err.status || 400).json({ msg: "not found" });

    const userCount = await User.find().countDocuments();

    return res
      .status(Constants.RESPONSE_CODES.REQUEST_OK)
      .send(ResponseTemplate.customDataTemplate(user, userCount, "user list"));
  } catch (err) {
    res
      .status(err.status || 400)
      .send(
        ResponseTemplate.badRequestTemplate(err.message, err.status || 400)
      );
  }
};

module.exports = {
  getReporters,
  userList,
};
