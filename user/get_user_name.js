const User = require("../models/user");
const Strings = require("../utils/string");
const Constants = require("../utils/constants.json");
const ResponseTemplate = require("../utils/response_template");

const getUserName = async (req, res) => {
  try {
    const getName = await User.find({}, { _id: 0, name: 1 });

    if (!getName.length) {
      let err = new Error("Not Found");
      err.status = Constants.RESPONSE_CODES.NOT_FOUND;
      throw err;
    } else {
      return res
        .status(Constants.RESPONSE_CODES.REQUEST_OK)
        .send(ResponseTemplate.dataTemplate(getName, `user names`));
    }
  } catch (err) {
    res
      .status(Constants.RESPONSE_CODES.NOT_FOUND)
      .send(
        ResponseTemplate.badRequestTemplate(
          Strings.ERROR_MESSAGES.DATA_NOT_FOUND,
          Constants.RESPONSE_CODES.NOT_FOUND
        )
      );
  }
};

module.exports = {
  getUserName,
};
