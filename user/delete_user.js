const User = require("../models/user");
const attendance = require("../models/attendance");
const Activity = require("../models/activity_log");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");
const { deleteUserLog } = require("../utils/activity_log");

const deleteUser = async (req, res, next) => {
  try {
    if (req.user.role !== "admin") {
      let err = new Error(`Not allowed.`);
      err.status = Constants.RESPONSE_CODES.NOT_FOUND;
      throw err;
    }

    const userDetails = await User.findOne({ _id: req.params.id });
    if (!userDetails) {
      let err = new Error(`User not found.`);
      err.status = Constants.RESPONSE_CODES.NOT_FOUND;
      throw err;
    }

    if (userDetails.role === "reporter") {
      await User.update(
        { reporter_id: userDetails._id },
        { $set: { reporter_id: req.user._id } },
        { multi: true }
      );
    }

    const deleteUser = await User.remove({ id: req.params.id });

    await attendance.remove({ employee_id: userDetails.employee_id });

    await Activity.remove({ employee_id: userDetails.employee_id });

    if (deleteUser.deletedCount) {
      await deleteUserLog(req.user.name, req.user.employeeId, userDetails.name);
      res.status(200).json("user deleted successfully");
    } else {
      res.status(400).json("cannot delete user");
    }
  } catch (err) {
    res
      .status(err.status || 400)
      .send(
        ResponseTemplate.badRequestTemplate(err.message, err.status || 400)
      );
  }
};

module.exports = {
  deleteUser,
};
