const User = require("../models/user");
const Strings = require("../utils/string");
const Constants = require("../utils/constants");
const ResponseTemplate = require("../utils/response_template");
const { body, validationResult } = require("express-validator");

const { editUserLog } = require("../utils/activity_log");

const editUser = async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      error: errors.array()[0].msg,
    });
  }

  try {
    if (req.user.role !== "admin") {
      let err = new Error("you are not allowed");
      err.status = Constants.RESPONSE_CODES.UN_AUTHORIZED;
      throw err;
    }

    const user = await User.findOne({ _id: req.params.userId });

    const updatedUser = await User.update(
      { _id: req.params.userId },
      { $set: req.body },
      { new: true, useFindAndModify: false }
    );

    if (user.role === "reporter" && req.body.role === "employee") {
      await User.update(
        { reporter_id: user._id },
        { $set: { reporter_id: req.user._id } },
        { multi: true }
      );
    }
    await editUserLog(req.user.name, req.user.employeeId, user.name);

    const updatedUserDetails = await User.findOne({ _id: req.params.userId });

    return res.status(200).json(updatedUserDetails);
  } catch (err) {
    res
      .status(err.status || 400)
      .send(
        ResponseTemplate.badRequestTemplate(err.message, err.status || 400)
      );
  }
};
module.exports = {
  editUser,
};
