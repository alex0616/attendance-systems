require('dotenv').config()
const express =  require("express");
const app = express();
var cookieParser = require('cookie-parser')
var cors = require('cors')
const mongoose = require("mongoose");

// Middlewares
app.use(express.json()) 
app.use(cookieParser())
app.use(cors())


// routes
const authRoute = require("./routes/auth");
const userRoute = require("./routes/user");
const userCountroRoute = require("./routes/userControls")

const designation = require("./api/designation");

const attendance = require("./api/attendance")

const user = require("./api/user")

const activity = require("./api/activity_log")

// my routes
app.use(authRoute);
app.use(userRoute);
app.use(userCountroRoute);
app.use(designation);
app.use(attendance);
app.use(user);
app.use(activity);



mongoose
  .connect(process.env.DATABASE,{
            useNewUrlParser: true,
            useFindAndModify: false,
            useCreateIndex: true,
            useUnifiedTopology: true
        })
  .then(result => {
    app.listen(3001);
    console.log("connected");
  })
