const Constants = require("../utils/constants");
const Strings = require("../utils/string");

module.exports = {
  successTemplate(data, message = "", source = "database") {
    return {
      status: Constants.RESPONSE_CODES.REQUEST_OK,
      // firstTimeLogin: true,
      data,
      message,
      source,
      success: true,
    };
  },

  successTemplate1(data, message = "", source = "database") {
    return {
      status: Constants.RESPONSE_CODES.REQUEST_OK,
      // firstTimeLogin: false,
      data,
      message,
      source,
      success: true,
    };
  },

  badRequestTemplate(message, statusCode, source = "database") {
    return {
      // statusCode: Constants.RESPONSE_CODES.BAD_REQUEST,
      statusCode,
      message,
      source,
      success: false,
    };
  },

  errorTemplate(code, message, error) {
    return {
      statusCode: code,
      body: JSON.stringify({ error_code: code, message, success: false }),
    };
  },
  dataNotFoundTemplate() {
    return {
      statusCode: Constants.RESPONSE_CODES.NOT_FOUND,
      message: Strings.ERROR_MESSAGES.DATA_NOT_FOUND,
      success: false,
    };
  },
  dataTemplate(data, message = "", source = "database") {
    return {
      statusCode: Constants.RESPONSE_CODES.REQUEST_OK,
      data,
      message,
      source,
      success: true,
    };
  },
  customDataTemplate(data,count, message = "", source = "database") {
    return {
      statusCode: Constants.RESPONSE_CODES.REQUEST_OK,
      data,
      count,
      message,
      source,
      success: true,
    };
  }
};
