const ActivityLog = require("../models/activity_log");

let logDate = () => {
  var date = new Date().toJSON().slice(0, 10);
  var currentDate =
    date.slice(8, 10) + "/" + date.slice(5, 7) + "/" + date.slice(0, 4);
  return currentDate;
};

let logTime = () => {
  var dates = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
  var todate = dates.split("/");
  var toDate = todate[1] + "/" + todate[0] + "/" + todate[2];
  var logTime = toDate.split(", ").pop();
  return logTime;
};

let postAction = "POST";

let updateAction = "UPDATE";

let deleteAction = "DELETE";

const createLog = async (name, employeeId, logDate, action, sentence) => {
  await ActivityLog.create({
    name: name,
    employee_id: employeeId,
    date: logDate,
    action: action,
    sentence: sentence,
  });
};

const checkInLog = async (name, employeeId) => {
  const sentence = `${name} has checkedIn for ${logDate()} at ${logTime()} .`;

  await createLog(name, employeeId, logDate(), postAction, sentence);
};

const checkOutLog = async (name, employeeId) => {
  const sentence = `${name} has checkedOut for ${logDate()} at ${logTime()} .`;

  await createLog(name, employeeId, logDate(), updateAction, sentence);
};

const leaveLog = async (name, employeeId) => {
  const sentence = `${name} has marked leave for ${logDate()} at ${logTime()} .`;

  await createLog(name, employeeId, logDate(), postAction, sentence);
};

const editAttendanceLog = async (
  name,
  employeeName,
  employeeId,
  attendanceDate
) => {
  const sentence = `${name} has edited the attendance of ${employeeName} for ${attendanceDate} on ${logDate()} at ${logTime()} .`;
  await createLog(name, employeeId, logDate(), updateAction, sentence);
};

const signUpLog = async (name, employeeName, employeeId, logDate, logTime) => {
  const sentence = `${name} has created new account for ${employeeName} on ${logTime} at ${logTime} .`;
  await createLog(name, employeeId, logDate, postAction, sentence);
};

const signInLog = async (name, employeeId) => {
  const sentence = `${name} has logged in on ${logDate()} at ${logTime()} .`;
  await createLog(name, employeeId, logDate(), postAction, sentence);
};

const signOutLog = async (name, employeeId, date, time) => {
  const sentence = `${name} is logged out on ${date} at ${time} .`;
};

const editUserLog = async (name, employeeId, employeeName) => {
  const sentence = `${name} has edited the details of ${employeeName} on ${logDate()} at ${logTime()} .`;
  await createLog(name, employeeId, logDate(), updateAction, sentence);
};

const deleteUserLog = async (name, employeeId, employeeName) => {
  const sentence = `${name} has deleted ${employeeName}'s account on ${logDate()} at ${logTime()} .`;
  await createLog(name, employeeId, logDate(), deleteAction, sentence);
};

module.exports = {
  checkInLog,
  checkOutLog,
  leaveLog,
  editAttendanceLog,
  signUpLog,
  signInLog,
  signOutLog,
  editUserLog,
  deleteUserLog,
};
