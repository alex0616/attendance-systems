const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  const token = req.header("Authentication");

  if (!token) return res.status(400).json({ msg: "token missing" });

  const newToken = token.replace("Bearer ", "");

  const decode = jwt.verify(newToken, process.env.SECRET);

  if (!decode) return res.status(401).json({ msg: "token invalid" });

  req.user = decode.payLoad;

  next();
};
